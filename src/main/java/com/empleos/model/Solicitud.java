package com.empleos.model;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import io.swagger.v3.oas.annotations.media.Schema;

@Entity
@Table(name="solicitudes")
public class Solicitud {
		
		@Id
		@GeneratedValue(strategy = GenerationType.IDENTITY)
		Integer idSolicitud;
	 
		@Column(name = "fecha", nullable = false)
		private LocalDateTime fecha;
		
		@Column(name = "archivo", nullable = false, length = 250)
		private String archivo;
		
		@Column(name = "comentarios", nullable = false, length = 1000)
		private String comentarios;

		@Schema(description = "Datos de la vacancia...")
		@ManyToOne
		@JoinColumn(name = "id_vacante", nullable = false, foreignKey = @ForeignKey(name = "FK_solicitud_vacante"))
		private Vacante vacante;

	    @ManyToOne
		@JoinColumn(name = "id_usuario", nullable = false, foreignKey = @ForeignKey(name = "FK_solicitud_usuario"))
		private Usuario usuario;

			public Integer getIdSolicitud() {
				return idSolicitud;
			}

			public void setIdSolicitud(Integer idSolicitud) {
				this.idSolicitud = idSolicitud;
			}

			public LocalDateTime getFecha() {
				return fecha;
			}

			public void setFecha(LocalDateTime fecha) {
				this.fecha = fecha;
			}

			public String getArchivo() {
				return archivo;
			}

			public void setArchivo(String archivo) {
				this.archivo = archivo;
			}

			public String getComentarios() {
				return comentarios;
			}

			public void setComentarios(String comentarios) {
				this.comentarios = comentarios;
			}

			public Vacante getVacante() {
				return vacante;
			}

			public void setVacante(Vacante vacante) {
				this.vacante = vacante;
			}

			public Usuario getUsuario() {
				return usuario;
			}

			public void setUsuario(Usuario usuario) {
				this.usuario = usuario;
			}
	        
}