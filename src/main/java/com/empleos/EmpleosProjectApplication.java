package com.empleos;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EmpleosProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(EmpleosProjectApplication.class, args);
	}

}
