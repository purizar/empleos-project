package com.empleos.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.empleos.model.Categoria;


public interface ICategoriaRepository extends JpaRepository<Categoria, Integer>{

}
