package com.empleos.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.empleos.model.Vacante;

public interface IVacanteRepository extends JpaRepository<Vacante, Integer>{

}
