package com.empleos.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.empleos.model.Solicitud;
import com.empleos.service.ISolicitudService;

@RestController
@RequestMapping("/solicitudes")
public class SolicitudController {
	
	@Autowired
	private ISolicitudService service;
	
	@GetMapping
	public ResponseEntity<List<Solicitud>> listar() throws Exception{
		List<Solicitud> lista = service.listar();
		return new ResponseEntity<List<Solicitud>>(lista, HttpStatus.OK);		
	}
	
	@PostMapping
	public ResponseEntity<Void> guardar(@RequestBody Solicitud solicitud ) {
		
		service.guardar(solicitud);
		return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
	}
	
	@PutMapping
	public ResponseEntity<Solicitud> modificar(@RequestBody Solicitud solicitud){
		Solicitud obj = service.modificar(solicitud);
		return new ResponseEntity<Solicitud>(obj, HttpStatus.OK);
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<Void> eliminar(@PathVariable("id") Integer id){
		Solicitud obj = service.buscarPorId(id);
		
		service.eliminar(id);
		return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<Solicitud> buscarPorId(@PathVariable("id") Integer id) throws Exception{
		Solicitud obj = service.buscarPorId(id);

		return new ResponseEntity<Solicitud>(obj, HttpStatus.OK);
	}
	

}
