package com.empleos.controller;

import java.net.URI;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.empleos.model.Categoria;
import com.empleos.service.ICategoriaSevice;

@RestController
@RequestMapping("/categorias")
public class CategoriaController {
	
	@Autowired
	private ICategoriaSevice service;
	
	@GetMapping
	public ResponseEntity<List<Categoria>> listar() throws Exception{
		List<Categoria> lista = service.listar();
		return new ResponseEntity<List<Categoria>>(lista, HttpStatus.OK);		
	}
	
	@PostMapping
	public ResponseEntity<Void> guardar(@RequestBody Categoria categoria ) {
		
		service.guardar(categoria);
		return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
	}
	
	@PutMapping
	public ResponseEntity<Categoria> modificar(@RequestBody Categoria categoria){
		Categoria obj = service.modificar(categoria);
		return new ResponseEntity<Categoria>(obj, HttpStatus.OK);
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<Void> eliminar(@PathVariable("id") Integer id){
		Categoria obj = service.buscarPorId(id);
		
		service.eliminar(id);
		return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<Categoria> buscarPorId(@PathVariable("id") Integer id) throws Exception{
		Categoria obj = service.buscarPorId(id);

		return new ResponseEntity<Categoria>(obj, HttpStatus.OK);
	}
	

	
}
