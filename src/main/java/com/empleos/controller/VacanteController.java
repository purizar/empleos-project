package com.empleos.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.empleos.model.Vacante;
import com.empleos.service.IVacanteService;

@RestController
@RequestMapping("/vacantes")
public class VacanteController {
	
	@Autowired
	private IVacanteService service;
	
	@GetMapping
	public ResponseEntity<List<Vacante>> listar() throws Exception{
		List<Vacante> lista = service.listar();
		return new ResponseEntity<List<Vacante>>(lista, HttpStatus.OK);		
	}
	
	@PostMapping
	public ResponseEntity<Void> guardar(@Valid @RequestBody Vacante vacante ) {
		
		service.guardar(vacante);
		return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
	}
	
	@PutMapping
	public ResponseEntity<Vacante> modificar(@RequestBody Vacante vacante){
		Vacante obj = service.modificar(vacante);
		return new ResponseEntity<Vacante>(obj, HttpStatus.OK);
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<Void> eliminar(@PathVariable("id") Integer id){
		Vacante obj = service.buscarPorId(id);
		
		service.eliminar(id);
		return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<Vacante> buscarPorId(@PathVariable("id") Integer id) throws Exception{
		Vacante obj = service.buscarPorId(id);

		return new ResponseEntity<Vacante>(obj, HttpStatus.OK);
	}
	

}
