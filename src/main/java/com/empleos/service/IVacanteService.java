package com.empleos.service;

import java.util.List;

import com.empleos.model.Categoria;
import com.empleos.model.Vacante;

public interface IVacanteService {
	
	void guardar(Vacante vacante);
	
	List<Vacante> listar();
	
	Vacante buscarPorId(Integer idVacante);	
	
	void eliminar (Integer idVacante);
	
	Vacante modificar(Vacante vacante);
}
