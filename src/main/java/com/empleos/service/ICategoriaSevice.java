package com.empleos.service;

import java.util.List;



import com.empleos.model.Categoria;

public interface ICategoriaSevice {
	
	void guardar(Categoria categoria);
	
	List<Categoria> listar();
	
	Categoria buscarPorId(Integer idCategoria);	
	
	void eliminar (Integer idCategoria);
	
	Categoria modificar(Categoria categoria);

}
