package com.empleos.service;

import java.util.List;

import com.empleos.model.Solicitud;

public interface ISolicitudService {
	
	void guardar(Solicitud solicitud);
	
	List<Solicitud> listar();
	
	Solicitud buscarPorId(Integer idSolicitud);	
	
	void eliminar (Integer idSolicitud);
	
	Solicitud modificar(Solicitud solicitud);
}
