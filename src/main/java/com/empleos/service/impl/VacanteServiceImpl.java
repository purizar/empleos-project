package com.empleos.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.empleos.model.Vacante;
import com.empleos.repository.IVacanteRepository;
import com.empleos.service.IVacanteService;

@Service
public class VacanteServiceImpl implements IVacanteService {

	@Autowired
	private IVacanteRepository repo;

	@Override
	public void guardar(Vacante vacante) {
		// TODO Auto-generated method stub
		repo.save(vacante);
	}

	@Override
	public List<Vacante> listar() {
		// TODO Auto-generated method stub
		 List<Vacante> lista = repo.findAll();
		
		return lista;
	}

	@Override
	public Vacante buscarPorId(Integer idVacante) {
		// TODO Auto-generated method stub
		Optional<Vacante> optional= repo.findById(idVacante);
		
		if(optional.isPresent()) {
			return optional.get();
		}
		
		return null;
	}

	@Override
	public void eliminar(Integer idVacante) {
		// TODO Auto-generated method stub
		repo.deleteById(idVacante);
	}

	@Override
	public Vacante modificar(Vacante vacante) {
		// TODO Auto-generated method stub
		Vacante vac = repo.save(vacante);
		
		return vac;
	}

}
