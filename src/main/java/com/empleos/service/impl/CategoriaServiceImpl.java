package com.empleos.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.empleos.model.Categoria;
import com.empleos.repository.ICategoriaRepository;
import com.empleos.service.ICategoriaSevice;

@Service
public class CategoriaServiceImpl implements ICategoriaSevice {
	
	@Autowired
	private ICategoriaRepository repo;

	@Override
	public void guardar(Categoria categoria) {
		// TODO Auto-generated method stub
		repo.save(categoria);
	}

	@Override
	public List<Categoria> listar() {
		// TODO Auto-generated method stub
		 List<Categoria> lista = repo.findAll();
		
		return lista;
	}

	@Override
	public Categoria buscarPorId(Integer idCategoria) {
		// TODO Auto-generated method stub
		Optional<Categoria> optional= repo.findById(idCategoria);
		
		if(optional.isPresent()) {
			return optional.get();
		}
		
		return null;
	}

	@Override
	public void eliminar(Integer idCategoria) {
		// TODO Auto-generated method stub
		repo.deleteById(idCategoria);
	}

	@Override
	public Categoria modificar(Categoria categoria) {
		// TODO Auto-generated method stub
		Categoria cat = repo.save(categoria);
		
		return cat;
	}

}
