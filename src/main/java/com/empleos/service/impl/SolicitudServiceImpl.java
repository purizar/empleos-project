package com.empleos.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.empleos.model.Solicitud;
import com.empleos.repository.ISolicitudRepository;
import com.empleos.service.ISolicitudService;

@Service
public class SolicitudServiceImpl implements ISolicitudService {

	@Autowired
	private ISolicitudRepository repo;

	@Override
	public void guardar(Solicitud solicitud) {
		// TODO Auto-generated method stub
		repo.save(solicitud);
	}

	@Override
	public List<Solicitud> listar() {
		// TODO Auto-generated method stub
		 List<Solicitud> lista = repo.findAll();
		
		return lista;
	}

	@Override
	public Solicitud buscarPorId(Integer idSolicitud) {
		// TODO Auto-generated method stub
		Optional<Solicitud> optional= repo.findById(idSolicitud);
		
		if(optional.isPresent()) {
			return optional.get();
		}
		
		return null;
	}

	@Override
	public void eliminar(Integer idSolicitud) {
		// TODO Auto-generated method stub
		repo.deleteById(idSolicitud);
	}

	@Override
	public Solicitud modificar(Solicitud solicitud) {
		// TODO Auto-generated method stub
		Solicitud sol = repo.save(solicitud);
		
		return sol;
	}

}
